#!/bin/bash
#Tries to find and sets the ip of your Roku device as $rokuip in $sourcefile
#You should probably assign a static ip to your Roku instead of using dhcp, this script finds your roku's ip

#sourcefile
#requires a line as such:
#rokuip="asdlfkj"
#for sed to replace
sourcefile="/home/$USER/Scripts/sources"

#maximum number of connections to make at a time
maxConn=50

#time to wait for response
timeout="1"

#the inclusive bounds for the 4th field of the ipv4 address, this should be set to the dhcp range
#max - min is the number of connection this script will make
min=1
max=254

#Don't touch
#Gets the first 3 numbers of your ipv4 address, ie. 192.168.1
baseIp=$(ip addr show | grep "inet 192.168" | awk '{print $2}' | cut -f "1 2 3" -d.)

#will echo the Roku's ip and set it in the sourcefile
function setIpIfRoku(){
	ip=$1
	
	resp=$(curl -s --connect-timeout $timeout "http://$ip:8060/query/device-info")

	if [[ $resp == *"Roku"* ]]; then
		echo $ip
		sed -i '/rokuip="/c\rokuip="'"$ip"'"' $sourcefile
	fi
}

for num in $(eval echo "{$min..$max..$maxConn}")
do
	inc=0
	while [ $((num + inc)) -le $max -a $inc -lt $maxConn ]
	do
		fieldFour=$((num + inc))
		setIpIfRoku "$baseIp.$fieldFour" &
		let "inc++"
	done
	wait
done

exit
