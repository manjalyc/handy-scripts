#!/bin/bash
commonexclude="--exclude '.steam' --exclude 'Git/linux-next' --exclude 'Git/burstcoin' --exclude '.local' --exclude '.cache' --exclude 'Videos/' --exclude 'snap/' --exclude '.eclipse' --exclude '.npm' --exclude '.themes' --exclude '.thumbnails' --exclude 'Git/papirus-icon-theme' --exclude '.wine' --exclude '.gem' --exclude '.icons' --exclude 'common/xowa' --exclude 'Cyriac/Now/DEFCON' --exclude 'Git/creepMiner'"
commoncommand="rsync -uav --no-l $(eval echo $commonexclude) --ignore-errors --delete --delete-excluded --log-file"


mkdir -p files
$commoncommand './logs/files' /mnt/files .
mkdir -p workspaces
$commoncommand './logs/workspaces' /mnt/workspaces .
mkdir -p common
$commoncommand './logs/common' /mnt/common .
"""
mkdir -p windows
rsync -uav --no-l --log-file './logs/files' $(eval echo $commonexclude) --ignore-errors --delete --delete-excluded /mnt/windows .
"""

mkdir -p arch-xfce
$commoncommand './logs/arch-xfce' /mnt/arch-xfce/home/cmnine ./arch-xfce
mkdir -p ubuntu
$commoncommand './logs/ubuntu' /mnt/ubuntu/home/cmten ./ubuntu
mkdir -p arch-gnome
$commoncommand './logs/arch-gnome' /home/cmeight ./arch-gnome
