#!/bin/bash
#Downloads a This American Life Episode
#requires zenity and wget

episode=$(zenity --entry --title="Aria2c" --text="Episode #:")
DIRECTORY=`cat /tmp/aria2ctorrentdir`
DIRECTORY=$(zenity --directory --file-selection --filename="$DIRECTORY" --title "Choose Ouput Directory: ")
echo $DIRECTORY > /tmp/aria2ctorrentdir
if [ -d "$DIRECTORY" ]; then
	notify-send  -t 3000 -i bash "Started TAL Download" $episode
	#wget -O "$DIRECTORY/$episode.mp3" stream.thisamericanlife.org/$episode/$episode.mp3 
	curl -o "$episode.mp3" "podcast.thisamericanlife.org/podcast/$episode.mp3"
	#ffmpeg -y -i "http://stream.thisamericanlife.org/${episode}/stream/${episode}_64k.m3u8" "$DIRECTORY/$episode.mp3"
	#>635 use podcast.thisamericanlife.org, otherwise use stream.thisamericanlife.org
	notify-send  -t 3000 -i bash "TAL Download Completed" $episode
else
	notify-send -t 3000 -i system-error "TAL: Canceled"
fi
