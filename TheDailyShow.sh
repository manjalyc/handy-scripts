#!/bin/bash
#Launches the most recent (1st to 5th) episode of The Daily Show on a Roku
#requires zenity to be installed
rokuip="YOUR ROKU IP"

#this line sources my rokuip, can be safely removed or ignored
source ~/Scripts/sources

selection=$(zenity --entry --title="The Daily Show" --text="Episode # (1-5):")

if [ "$selection" -ge "1" ] &&  [ "$selection" -le "5" ]
then
	curl -X POST "http://$rokuip:8060/keypress/Home"
	sleep 3
	curl -X POST "http://$rokuip:8060/launch/63344"
	sleep 10
	curl -X POST "http://$rokuip:8060/keypress/Down"
	sleep 1
	curl -X POST "http://$rokuip:8060/keypress/Down"
	sleep 1
	#curl -X POST "http://$rokuip:8060/keypress/Right" #it depends on your variance now apparently
	sleep 1
	curl -X POST "http://$rokuip:8060/keypress/Select"
	sleep 8
	curl -X POST "http://$rokuip:8060/keypress/Down"
	selection=$((selection-1))
	for i in `seq $selection`; do
		sleep 1;
		curl -X POST "http://$rokuip:8060/keypress/Right";
	done
	sleep 1
	curl -X POST "http://$rokuip:8060/keypress/Select"
else
	zenity --error --title="Warning" --text="Invalid Input\nStopped." 
fi
