#!/bin/bash
#Burst 1TB.1
#A one-off tool to plot Burst at a reasonable pace
#Requires lsblk


burstLogDir="."

: '
This section is for me to set my logging dir, it can be deleted
If deleted logfiles will be set in whatever the current directory is
'
home=$(eval echo "~")
source $home/Scripts/sources


#name
name="Burst-1TB.1"
mntDir="/mnt/$name"
pblog="$burstLogDir/$name.pblog"
min=100000000
max=103787200
step=8192 #used for memory and step

#TODO: Delete THIS LINE
echo "Log File at: $pblog"

#colors
LRED='\033[1;31m'
RED='\033[0;31m'
BLUE='\033[0;34m'
NC='\033[0m'



new_min=$(tail -n 1 $pblog 2> /dev/null)
if ! [ -z "$new_min" ]; then
	printf "${LRED}Setting min to $new_min as found in $pblog${NC}\n"
	min=$new_min
fi

#location of the mdcct plotter
home=$(eval echo "~")
mdcct_home="$home/Git/mdcct"
for num in $(eval echo "{$min..$max..$step}"); do 
	#Check if mounted
	if lsblk | grep  $mntDir > /dev/null; then
		ig="ig"
	else
		sudo mount $mntDir #requires $mntDir in /etc/fstab and maybe sudo for automanagement
		if lsblk | grep  $mntDir > /dev/null; then
			echo "$name was automagically mounted at $mntDir"
		else
			echo "refusing to continue, nothing mounted at $mntDir"
			exit
		fi
	fi
	printf "${RED}$num:$step-->$max${BLUE}\n"
	echo "$num" >> $pblog
	date
	printf "$NC"
	$mdcct_home/plot -k 6680006054201820551 -x 1 -d "$mntDir" -s $num -n $step -m $step -t 4
done
