#!/bin/bash
ip link set wlo1 down
macchanger -A wlo1;
ip link set wlo1 up

current=$(macchanger -s wlo1 | grep "Current MAC:")
permanent=$(macchanger -s wlo1 | grep "Permanent MAC:")

echo $(logname)
su $(logname) -c 'notify-send -i "network-wired-symbolic" "MAC Changer" "'"$permanent"'"'
su $(logname) -c 'notify-send -i "network-wired-symbolic" "MAC Changer" "'"$current"'"'
