#!/bin/bash
partition="/dev/sda10"
mountpoint="/mnt/nixi3"
NIXOS_VERSION="19.09"


function maintainSudo(){
	sudo echo "."
}

echo "Installing v${NIXOS_VERSION} to partition $partition, on mountpoint $mountpoint [Ensure partition is already formatted and ready]"
sudo echo "Granted Sudo Permissions"
curl https://nixos.org/nix/install | sh
. $HOME/.nix-profile/etc/profile.d/nix.sh
maintainSudo
nix-channel --add "https://nixos.org/channels/${NIXOS_VERSION}" nixpkgs
nix-channel --update
maintainSudo
nix-env -iE "_: with import <nixpkgs/nixos4> { configuration = {}; }; with config.system.build; [ nixos-generate-config nixos-install nixos-enter manual.manpages ]"
sudo `which nixos-generate-config` --root "$mountpoint"

read -p "Generic Configuration Installed, Press Enter to Continue Installing NixOS"

