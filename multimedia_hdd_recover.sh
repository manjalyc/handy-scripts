#!/bin/bash

#hint use lsblk or blkid to identify your device
#target_device="/dev/sdc"
#target_partition="/dev/sdc2"
mount_partition_to="/mnt/recovery"
save_data_to="/mnt/windows/HDD Recovery"


if [ "$EUID" -ne 0 ]
  then echo "Please run as root (ie. with sudo)"
  exit
fi

#fail if lsblk, blkid, or rsync is not detected
echo "-----------------------------------------"
echo "Checking for lsblk, blkid, rsync"
echo "-----------------------------------------"
lsblk --help 1>/dev/null
if [ $? -ne 0 ]; then echo "ERR: u need to install lsblk"; exit; fi
blkid --help 1>/dev/null
if [ $? -ne 0 ]; then echo "ERR: u need to install blkid";  exit; fi
rsync --help 1>/dev/null
if [ $? -ne 0 ]; then echo "ERR: u need to install rsync";  exit; fi
echo "Success!"





UUID=`blkid -o value -s UUID /dev/sdc1`
save_data_to="$save_data_to/$UUID"

echo "-----------------------------------------"
echo "lsblk Output"
echo "-----------------------------------------"
lsblk


echo "-----------------------------------------"
echo "Step 0a: Initialize Variables"
echo "-----------------------------------------"
echo -ne "Enter Target Device (ie. /dev/sdc): "
read target_device

echo -ne "Enter Target Partition (ie. /dev/sdc1): "
read target_partition


echo "-----------------------------------------"
echo "Step 0b: Double Check"
echo "-----------------------------------------"
echo "Target Partition: $target_partition"
echo "Target Partition Will be Mounted to: $mount_partition_to"
echo "UUID of partition: $UUID"
echo "Data will be saved to: $save_data_to"
read -p "Press Enter To Continue (Ctrl-C to quit)..."

echo "-----------------------------------------"
echo "Step 1: Making Folders & Mounting"
echo "-----------------------------------------"
mkdir -p $mount_partition_to
mkdir -p $save_data_to
umount $target_partition 2>/dev/null
mount $target_partition $mount_partition_to
lsblk

echo "-----------------------------------------"
echo "Step 2: Copying Multimedia"
echo "-----------------------------------------"

# formats.txt, includes images and videos
# .tif
# .tiff
# .bmp
# .jpg
# .jpeg
# .gif
# .png
# .webm
# .mkv
# .flv
# .flv
# .vob
# .ogv
# .ogg
# .drc
# .gif
# .gifv
# .mng
# .avi
# .MTS
# .M2TS
# .TS
# .mov
# .qt
# .wmv
# .yuv
# .rm
# .rmvb
# .asf
# .amv
# .mp4
# .m4p
# .m4v
# .mpg
# .mp2
# .mpeg
# .mpe
# .mpv
# .mpg
# .mpeg
# .m2v
# .m4v
# .svi
# .3gp
# .3g2
# .mxf
# .roq
# .nsv
# .flv
# .f4v
# .f4p
# .f4a
# .f4b

# format.py
# final = ""
# for line in open('formats.txt','r'):
# 	line = line.strip()
# 	toPrint = ' --include="*.'
# 	for char in line[1:]:
# 		toPrint += f'[{char.lower()}{char.upper()}]'
# 	toPrint += '"'
# 	print(line, toPrint)
# 	final += toPrint
# print(final)

rsync -uav --prune-empty-dirs --include="*/" --include="*.[tT][iI][fF]" --include="*.[tT][iI][fF][fF]" --include="*.[bB][mM][pP]" --include="*.[jJ][pP][gG]" --include="*.[jJ][pP][eE][gG]" --include="*.[gG][iI][fF]" --include="*.[pP][nN][gG]" --include="*.[wW][eE][bB][mM]" --include="*.[mM][kK][vV]" --include="*.[fF][lL][vV]" --include="*.[fF][lL][vV]" --include="*.[vV][oO][bB]" --include="*.[oO][gG][vV]" --include="*.[oO][gG][gG]" --include="*.[dD][rR][cC]" --include="*.[gG][iI][fF]" --include="*.[gG][iI][fF][vV]" --include="*.[mM][nN][gG]" --include="*.[aA][vV][iI]" --include="*.[mM][tT][sS]" --include="*.[mM][22][tT][sS]" --include="*.[tT][sS]" --include="*.[mM][oO][vV]" --include="*.[qQ][tT]" --include="*.[wW][mM][vV]" --include="*.[yY][uU][vV]" --include="*.[rR][mM]" --include="*.[rR][mM][vV][bB]" --include="*.[aA][sS][fF]" --include="*.[aA][mM][vV]" --include="*.[mM][pP][44]" --include="*.[mM][44][pP]" --include="*.[mM][44][vV]" --include="*.[mM][pP][gG]" --include="*.[mM][pP][22]" --include="*.[mM][pP][eE][gG]" --include="*.[mM][pP][eE]" --include="*.[mM][pP][vV]" --include="*.[mM][pP][gG]" --include="*.[mM][pP][eE][gG]" --include="*.[mM][22][vV]" --include="*.[mM][44][vV]" --include="*.[sS][vV][iI]" --include="*.[33][gG][pP]" --include="*.[33][gG][22]" --include="*.[mM][xX][fF]" --include="*.[rR][oO][qQ]" --include="*.[nN][sS][vV]" --include="*.[fF][lL][vV]" --include="*.[fF][44][vV]" --include="*.[fF][44][pP]" --include="*.[fF][44][aA]" --include="*.[fF][44][bB]" --exclude="*" "$mount_partition_to" "$save_data_to"


echo "-----------------------------------------"
echo "Step 3: Unmounting & Ejecting"
echo "-----------------------------------------"
umount "$target_partition"
eject "$target_device"
lsblk
