# handy-scripts

A collection of various useful everyday scripts, from launching shows on a Roku to torrenting to mounting Apple devices in non-gvfs-afc environments.

- A lot of these scripts were originally designed to be keybinded in a Desktop Environment, and hence require zenity for user input.

- The required packages for a script to work should be in the first couple of lines of the script.

- Some scripts may source ~/Scripts/sources, this line can safely be removed (or ignored) unless otherwise stated in the script.