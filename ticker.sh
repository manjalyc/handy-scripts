#!/bin/bash
#A script to print out ticker information from bitfinex
#https://docs.bitfinex.com/v1/reference#rest-public-ticker

if [[ "$*" == *"-h"* ]]; then
	echo "A Script to print out ticker information from bitfiniex"
	echo "https://docs.bitfinex.com/v1/reference#rest-public-ticker"
	echo "Usage: "
	echo "  ./ticker.sh symbol key"
	echo "Ex: "
	echo "  ./ticker.sh btcusd last_price"
	exit
fi

response=$(curl -s https://api.bitfinex.com/v1/pubticker/$1)
echo $response | jq -r ".$2"
