#!/bin/bash
#periodically simulates pressing the home button on a Roku :)>

rokuip="YOUR ROKU IP"

#this line sources my rokuip, can be safely removed or ignored
source ~/Scripts/sources

echo "rokuip: $rokuip"
while true;
do
	curl -X POST "http://$rokuip:8060/keypress/Home"
	echo "SENT"
	sleep 150
done
