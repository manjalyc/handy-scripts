#!/bin/bash
#Set up a new tor relay


: ' One-time setup, run your respective init system line in a terminal
#For systemd
echo "ALL ALL=NOPASSWD: /usr/bin/systemctl start tor" | sudo tee --append /etc/sudoers 1> /dev/null; echo "ALL ALL=NOPASSWD: /usr/bin/systemctl stop tor" | sudo tee --append /etc/sudoers 1> /dev/null
#openRC
echo "ALL ALL=NOPASSWD: /sbin/rc-service tor start" | sudo tee --append /etc/sudoers 1> /dev/null; echo "ALL ALL=NOPASSWD: /sbin/rc-service tor stop" | sudo tee --append /etc/sudoers 1> /dev/null
'
#systemd 
#required in /etc/sudoers: ALL ALL=NOPASSWD: /usr/bin/systemctl start tor
#required in /etc/sudoers: ALL ALL=NOPASSWD: /usr/bin/systemctl stop tor

#openRC
#required in /etc/sudoers: ALL ALL=NOPASSWD: /sbin/rc-service tor start
#required in /etc/sudoers: ALL ALL=NOPASSWD: /sbin/rc-service tor stop

if [ $(pidof systemd > /dev/null; echo "$?") -eq "0" ]; then
	sudo systemctl stop tor
	sudo systemctl start tor
else
	sudo rc-service tor stop
	sudo rc-service tor start
fi

notify-send -i bash "Onion Routing" "New Relay Opened"
torsocks curl https://check.torproject.org/ 1> /tmp/tmp.html
ipaddr=""
while [[ -z $ipaddr ]]; do
	torsocks wget https://check.torproject.org/ -O /tmp/tmp.html
	ipaddr=`cat /tmp/tmp.html | grep "Your IP address appears to be" | awk -F'strong\>' '{print $2}' 2>/dev/null | awk -F'\<' '{print $1}' 2>/dev/null`
done
notify-send -i bash "Onion Routing" "IP Address: $ipaddr"
notify-send -i bash "SOCKSv5 DNS Resolution enabled" "127.0.0.1:9050"
rm *wget*log*
