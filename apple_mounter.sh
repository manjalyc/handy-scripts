#!/bin/bash
#Mounts apple devices, and their app folders
#run './apple_mounter.sh -h' for help

#Currently assumes only 1 apple device is plugged in
#requires app-pda/usbmuxd app-pda/libimobiledevice app-pda/ifuse app-pda/ideviceinstaller

#Where to mount the device, Media will be mounted in $appleMountDir/Media, apps in their respective subfolder at $appleMountDir/Apps/
appleMountDir="/mnt/apple"

#sources and sets my appleMountDir, you can safely remove this line
source ~/Scripts/sources 

#Users/Groups that can access the $appleMountDir, and subdirs.
permitted=$(whoami):root

#colors for output
LRED='\033[1;31m'
RED='\033[0;31m'
BLUE='\033[0;34m'
CYAN='\033[0;36m'
NC='\033[0m'

#read and set command line options
mode=""
exitPrompt=false
if [[ "$*" == *"-h"* ]]; then
	echo "Usage: "
	echo "  ./apple_mounter.sh [OPTION]"
	echo ""
	echo "options: "
	echo -e "\t-m\tmounts your apple device"
	echo -e "\t-u\tunmounts your apple device"
	echo -e "\t-h\tshow this help screen"
	echo -e "\t-p\tbefore exiting, display a prompt"
	exit
elif [[ "$*" == *"-m"* ]]; then
	mode="m"
elif [[ "$*" == *"-u"* ]]; then
	mode="u"
fi
if [[ "$*" == *"-p"* ]]; then
	exitPrompt=true
fi

function quit(){
	if $exitPrompt; then
		read -p "Press [Enter] to exit..."
	fi
	exit
}


if [ "$mode" == "" ]; then
	read -n 1 -p "[m] to mount your Apple Device, [u] to unmount, anything else to quit: " mode
	echo ""
fi

device=$(idevicepair pair | grep -Po -m1 '(device\s)\K[^\s]*')
pairedList=$(idevicepair list)
if [[ "$device" == "found," ]]; then
	echo "${RED}Could not find a device, exiting${NC}"
	quit
fi




#If we're not paired with the device, pair with the device
#TODO add an option to give up here
while [[ "$pairedList" != *"$device"* ]] && [ "$mode" != "u" ]; do
	echo -e "${RED}Device not paired.${NC}"
	read -p "Press [Enter] when ready to pair..."
	idevicepair pair
	device=$(idevicepair pair | grep -Po -m1 '(device\s)\K[^\s]*')
	pairedList=$(idevicepair list)
done

#Get Sudo Permissions
echo "Retriving Sudo Permissions..."
sudo printf "${BLUE}...retrived${NC}\n"

if [ "$mode" == "u" ]; then
	echo -e "Unmounting ${CYAN}Media${NC} at $appleMountDir/Media..."
	sudo umount $appleMountDir/Media
	sudo rmdir $appleMountDir/Media
	for app in $(dir $appleMountDir/Apps); do
		echo -e "Unmounting ${BLUE}$app${NC} at $appleMountDir/Apps/$app..."
		sudo umount $appleMountDir/Apps/$app
		sudo rmdir $appleMountDir/Apps/$app
	done
	sudo rmdir $appleMountDir/Apps
elif [ "$mode" == "m" ]; then
	if mount | grep  $appleMountDir/Media > /dev/null; then
		echo -e "${LRED}Media already Mounted at $appleMountDir, exiting${NC}"
		quit
	fi

	sudo chmod 4755 /usr/bin/fusermount
	sudo mkdir -p $appleMountDir/Media
	sudo mkdir -p $appleMountDir/Apps

	sudo chown $permitted $appleMountDir
	sudo chmod 775 $appleMountDir

	sudo chown $permitted $appleMountDir/*
	sudo chmod 775 $appleMountDir/*

	ifuse $appleMountDir/Media

	if mount | grep  $appleMountDir/Media > /dev/null; then
		echo -e "Mounted ${CYAN}Media${NC} at $appleMountDir/Media"
	else
		echo -e "${RED}Unable to mount apple Media, aborting${NC}"
		quit
	fi

	for app in $(ideviceinstaller -l | awk '{print $1}'); do 
		if [[ $app != *":"* ]]; then

			#check if already mounted
			if mount | grep  $appleMountDir/Apps/$app > /dev/null; then
				echo -e "${LRED}$app is already mounted at $appleMountDir/Apps/$app, skipping...${NC}"
				continue
			fi
			
			sudo mkdir -p $appleMountDir/Apps/$app
			sudo chown $permitted $appleMountDir/Apps/$app
			sudo chmod 775 $appleMountDir/Apps/$app
			ifuse --documents $app "$appleMountDir/Apps/$app" 2> /dev/null
			#check exit status
			if [[ $? -eq 0 ]]; then
				#success
				echo -e "Mounted ${BLUE}$app${NC} to $appleMountDir/Apps/$app"
			elif [[ $? -eq 1 ]]; then
				#failed, remove directory
				sudo rmdir "$appleMountDir/Apps/$app"
			else
				echo -e "${RED}Unknown exit status: $? for $app${NC}"
			fi
		fi
	done
fi
quit
