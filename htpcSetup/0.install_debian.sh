#!/bin/bash
entryDir=$(echo $PWD)

#Prep Work
echo "[0] Prepping MAKE SURE YOU RUN THIS AS ROOT"
shopt -s expand_aliases
source ./variables
sudo mkdir -p $downloaddir $mountpoint
echo mountpoint: $mountpoint
echo devdir: $devdir
echo UUIDL $UUID
echo downloadir: $downloaddir
#cd $downloaddir

#Verify UUID = $devdir
# xs=$(sudo blkid $devdir)
# if [[ $UUID == *"$xs"* ]]; then
# 	echo "Verified UUID matched $devdir"
# else
# 	echo "Failed to verify UUID matched $devdir"
# fi

#Get debootstrap
unamer=$(uname -r)
if [[ $unamer == *"ARCH" ]]; then
	echo "[1] Detected Arch Linux, Installing debootstrap..."
	sudo pacman -S --needed --noconfirm debootstrap debian-archive-keyring
else
	echo false
fi

#debootstrap stage 1 base system
sudo umount $devdir $mountpoint
sudo mount $devdir $mountpoint
sudo debootstrap --arch amd64 $version $mountpoint $debootstraplink
sudo cp /usr/bin/qemu-amd64-static $mountpoint/usr/bin

#Generate Device Files
chrootRun "apt install makedev; mount none /proc -t proc; cd /dev; /sbin/MAKEDEV generic"

#Fstab
printf "$fstabLine" >> $mountpoint/etc/fstab
mkdir -p $mountpoint/mnt/windows $mountpoint/mnt/swindows $mountpoint/mnt/common $mountpoint/mnt/workspaces $mountpoint/mnt/archkde $mountpoint/mnt/archkdes $mountpoint/mnt/esp

#Time Zone, requires to be run as root
echo $timeZone > $mountpoint/etc/timezone
chrootRun "/sbin/dpkg-reconfigure -f noninteractive tzdata"


#Networking
#loopback
echo "#loopback" >> $mountpoint/etc/network/interfaces
echo "auto lo" >> $mountpoint/etc/network/interfaces
echo "iface lo inet loopback" >> $mountpoint/etc/network/interfaces
#dhcp
echo "#dhcp"
echo "auto $wired" >> $mountpoint/etc/network/interfaces
echo "iface $wired inet dhcp" >> $mountpoint/etc/network/interfaces
#wifi



#resolv.conf
echo "nameserver $nameserver1
" >> $mountpoint/etc/resolv.conf #cloudfare
echo "nameserver $nameserver2
" >> $mountpoint/etc/resolv.conf #google
#hostname * /etc/hosts
echo $hostname > $mountpoint/etc/hostname
echo "127.0.0.1 localhost" >> $mountpoint/etc/hosts
echo "127.0.1.1 $hostname" >> $mountpoint/etc/hosts
echo "::1     ip6-localhost ip6-loopback" >> $mountpoint/etc/hosts
echo "fe00::0 ip6-localnet" >> $mountpoint/etc/hosts
echo "ff00::0 ip6-mcastprefix" >> $mountpoint/etc/hosts
echo "ff02::1 ip6-allnodes" >> $mountpoint/etc/hosts
echo "ff02::2 ip6-allrouters" >> $mountpoint/etc/hosts
echo "ff02::3 ip6-allhosts" >> $mountpoint/etc/hosts

#Configure apt
echo "deb-src http://ftp.us.debian.org/debian $version main" >> $mountpoint/etc/apt/sources.list
echo "deb http://security.debian.org/ $version/updates main" >> $mountpoint/etc/apt/sources.list
echo "deb-src http://security.debian.org/ $version/updates main" >> $mountpoint/etc/apt/sources.list
echo "deb http://httpredir.debian.org/debian/ $version main contrib non-free" >> $mountpoint/etc/apt/sources.list
chrootRun "apt update"

#locales
chrootRun "apt-get --yes --force-yes install locales"
#chrootRun "dpkg-reconfigure locales"
# chrootRun '/sbin/update-locale "en_US.UTF-8 UTF-8"'
# chrootRun '/sbin/locale-gen --purge "en_US.UTF-8 UTF-8"'
# chrootRun 'PATH="$PATH:/sbin"; /sbin/dpkg-reconfigure --frontend noninteractive locales'

chrootRun 'PATH="$PATH:/sbin"; echo "locales locales/default_environment_locale select en_US.UTF-8" | debconf-set-selections'
chrootRun 'PATH="$PATH:/sbin"; echo "locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8" | debconf-set-selections
'
chrootRun 'rm "/etc/locale.gen"'
chrootRun 'PATH="$PATH:/sbin"; /sbin/dpkg-reconfigure --frontend noninteractive locales'
chrootRun "apt-get --yes --force-yes install linux-image-amd64"

cd $entryDir
chrootRun '/bin/tasksel install standard'

#wifi
chrootRun "apt-get --yes install lshw rfkill"
chrootRun "apt-get --yes update && apt-get --yes install firmware-iwlwifi"
chrootRun "apt-get --yes install network-manager nm-tray"
chrootRun "systemctl enable NetworkManager"


