#!/bin/bash
echo "RUN AS ROOT"
sudo echo $username
shopt -s expand_aliases
source variables


#i3
chrootRun "apt-get --yes install i3 rofi lxappearance qt5ct network-manager-gnome"

#ExtraUtils
chrootRun "apt-get --yes install compton redshift-gtk ttf-ubuntu-font-family xfce4-clipman udevil feh"
chrootRun "apt-get --yes install python3 python3-pip python3-numpy"

#i3lock
chrootRun "pip3 install Pillow"

#polybar
chrootRun "apt-get install cmake cmake-data libxcb-composite0-dev fonts-font-awesome fonts-firacode scrot"
#If that doesn't work install everything down below
#cmake cmake-data libcairo2-dev libxcb1-dev libxcb-ewmh-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-randr0-dev libxcb-util0-dev libxcb-xkb-dev pkg-config python-xcbgen xcb-proto libxcb-xrm-dev i3-wm libasound2-dev libmpdclient-dev libiw-dev libcurl4-openssl-dev libpulse-dev


#Basic .xinitrc
echo "xrdb -merge ~/.Xresources" >> $mountpoint/home/$username/.xinitrc
echo "#export QT_AUTO_SCREEN_SCALE_FACTOR=1.2" >> $mountpoint/home/$username/.xinitrc
echo "#QT_FONT_DPI=115 vym" >> $mountpoint/home/$username/.xinitrc
echo "#export QT_FONT_DPI=115 vym" >> $mountpoint/home/$username/.xinitrc
echo "#export GDK_SCALE=1.2" >> $mountpoint/home/$username/.xinitrc
echo "QT_QPA_PLATFORMTHEME=qt5ct" >> $mountpoint/home/$username/.xinitrc
echo "export QT_QPA_PLATFORMTHEME=qt5ct" >> $mountpoint/home/$username/.xinitrc
echo "exec i3" >> $mountpoint/home/$username/.xinitrc
