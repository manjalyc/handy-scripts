#!/bin/bash
source variables
chrootRun "passwd"
chrootRun "useradd -m -g users -G wheel -s /bin/bash $username"
printf "$cmdeb\tALL=(ALL:ALL) ALL" >> $mountpoint/etc/sudoers
