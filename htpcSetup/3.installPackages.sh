#!/bin/bash
echo "RUN AS ROOT"
sudo echo $username
shopt -s expand_aliases
source variables

fileManagers="dolphin thunar nautilus pcmanfm" 
terminals="mate-terminal lxterminal"
editors="gedit"
browsers=""

chrootRun "apt-get --yes update"
chrootRun "apt-get --yes clean"
chrootRun "apt-get --yes autoclean"
chrootRun "apt-get --yes autoremove"
chrootRun "apt-get --yes install "
chrootRun "DEBIAN_FRONTEND=noninteractive apt-get install --yes keyboard-configuration"
chrootRun "DEBIAN_FRONTEND=noninteractive apt-get install --yes xorg"


#Essentials

#Audio
chrootRun "apt-get --yes install pulseaudio pavucontrol alsa-utils"
#may need to run alsamixer to enable front panel jack detection

#Printing
chrootRun "apt-get --yes install cups hplip"
#may involve input
chrootRun "apt-get --yes install printer-driver-cups-pdf"

#Extras
chrootRun "apt-get --yes install $browsers $fileManagers $terminals $editors vlc cmus"

