#!/bin/bash
shopt -s expand_aliases
source ./variables

echo "MUST RUN AS ROOT"
sudo echo "mountpoint: $mountpoint"
ln -s $mountpoint/boot/config-* $mountpoint/boot/config
ln -s $mountpoint/boot/System.map-* $mountpoint/boot/System.map
ln -s $mountpoint/boot/initrd.img-* $mountpoint/boot/initrd.img
ln -s $mountpoint/boot/vmlinuz-* $mountpoint/boot/vmlinuz

echo "esp systemd dir: $systemdLoaderDir"
mkdir -p $systemdLoaderDir
cp $mountpoint/boot/config $systemdLoaderDir
cp $mountpoint/boot/System.map $systemdLoaderDir
cp $mountpoint/boot/initrd.img $systemdLoaderDir
cp $mountpoint/boot/vmlinuz $systemdLoaderDir

printf "title   Debian [$hostname on $version]\n" > $systemdConf
printf "linux   $systemdLoaderRelativeDir/vmlinuz\n" >> $systemdConf
printf "initrd  $systemdLoaderRelativeDir/initrd.img\n" >> $systemdConf

e2label $devdir $e2PartitionLabel
printf "options root=LABEL=$e2PartitionLabel\n" >> $systemdConf

echo ""
echo "$mountpoint/boot"
ls -alh --color $mountpoint/boot
echo ""
echo "$systemdLoaderDir"
ls -alh --color $systemdLoaderDir
echo ""
echo "$systemdConf"
cat $systemdConf
