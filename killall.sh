#!/bin/bash
#A panic script to just stop everything
amixer -q set Master mute
killall cava gnuplot_qt gnuplot Ravenfield.x86_64 cmus vlc popcorntime spotify devilspie vis conky hl2_linux hl2.sh cava & killall -9 kodi.bin steam steamwebhelper hl2.sh linux_client aces polybar desmume

#this line sources my rokuip, can be safely removed or ignored
source ~/Scripts/sources

curl -X POST "http://$rokuip:8060/keypress/Home"

kill $(ps -ef | grep ums.jar | grep java | awk -v N=2 '{print $N}')
kill $(ps -ef | grep emby | grep mono | awk -v N=2 '{print $N}')
sudo systemctl stop emby-server

