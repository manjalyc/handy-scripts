#!/bin/bash
#Downloads a magnet uri (a torrent)
#requires zenity and aria2c

magnet=$(zenity --entry --title="Aria2c" --text="Paste Magnet URI:")
DIRECTORY=`cat /tmp/aria2ctorrentdir`
DIRECTORY=$(zenity --directory --file-selection --filename=$DIRECTORY --title "Aria2c: Choose Ouput Directory")
echo $DIRECTORY > /tmp/aria2ctorrentdir
echo $DIRECTORY
echo $magnet
if [ -d "$DIRECTORY" ]; then
	echo $magnet
	aria2c  --dht-listen-port=4000-7000 -d $DIRECTORY --bt-force-encryption=true --bt-hash-check-seed=true --bt-min-crypto-level=arc4 --bt-require-crypto=true --seed-time=0 $magnet
else
	aria2c  --dht-listen-port=4000-7000 -d ~/Videos --bt-force-encryption=true  --bt-hash-check-seed=true --bt-min-crypto-level=arc4 --bt-require-crypto=true --seed-time=0 $magnet
fi
if [ $? -eq 0 ]; then
	notify-send  -t 500000000 -i bash "Download Completed" $magnet
else
	notify-send  -t 5000 -i bash "Download Failed"  "Exit Code $? for $magnet"
fi
